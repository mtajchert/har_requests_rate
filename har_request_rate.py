import argparse
import json
from datetime import datetime, timedelta
import os
from asciichartpy import plot

def parse_time(time_str):
    if time_str.endswith("s"):
        return timedelta(seconds=int(time_str[:-1]))
    elif time_str.endswith("m"):
        return timedelta(minutes=int(time_str[:-1]))
    elif time_str.endswith("h"):
        return timedelta(hours=int(time_str[:-1]))
    else:
        raise ValueError("Invalid time format. Use 's', 'm', or 'h' suffix.")

def parse_iso8601(date_str):
    return datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S.%f%z")

def analyze_har_file(har_path, window_time, window_increment, domain_filter=None, debug=False, show_graph=True):
    if not os.path.exists(har_path) or os.path.getsize(har_path) == 0:
        print(f"Error: The specified HAR file '{har_path}' does not exist or is empty.")
        return

    with open(har_path, "r", encoding="utf-8") as har_file:
        har_data = json.load(har_file)

    entries = har_data["log"]["entries"]

    # Sort entries based on 'startedDateTime'
    entries.sort(key=lambda entry: parse_iso8601(entry["startedDateTime"]))

    start_time = parse_iso8601(entries[0]["startedDateTime"])

    total_requests = len(entries)
    max_avg_requests = 0
    max_avg_window_start = None
    max_window_requests = 0

    current_window_start = start_time
    window_data = []

    while current_window_start <= parse_iso8601(entries[-1]["startedDateTime"]) - window_time:
        window_end = current_window_start + window_time
        requests_in_window = [entry for entry in entries if
                              current_window_start <= parse_iso8601(entry["startedDateTime"]) < window_end and
                              (not domain_filter or any(header["name"].lower() == "host" and domain_filter.lower() in header["value"].lower() for header in entry["request"]["headers"]))]
        num_requests_in_window = len(requests_in_window)
        avg_requests_in_window = num_requests_in_window / window_time.total_seconds()

        if avg_requests_in_window > max_avg_requests:
            max_avg_requests = avg_requests_in_window
            max_avg_window_start = current_window_start

        if num_requests_in_window > max_window_requests:
            max_window_requests = num_requests_in_window

        window_data.append(num_requests_in_window)

        if debug:
            print(f"Window Start: {current_window_start} | Window End: {window_end}, requests: {num_requests_in_window}")

        current_window_start += window_increment

    print(f"Total requests: {total_requests}")
    print(f"Window time: {window_time.total_seconds()} seconds")
    print(f"Window increment: {window_increment.total_seconds()} seconds")
    print(f"Average requests: {max_avg_requests}")
    print(f"Max requests in a single window: {max_window_requests}")

    if show_graph:
        print("\nNumber of Requests in Each Window:")
        print(plot(window_data, {'height': 10}))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Analyze Chrome HAR file of network requests with sliding window technique.")
    parser.add_argument("--har_path", type=str, help="Path to the Chrome HAR file", required=True)
    parser.add_argument("--window_time", type=parse_time, help="Window time (e.g., '1m', '20s', '1h')", required=True)
    parser.add_argument("--window_increment", type=parse_time, help="Window increment (e.g., '1m', '20s', '1h')", required=True)
    parser.add_argument("--domain_filter", type=str, help="Filter requests for a specific domain (use partial 'Host' header value)")
    parser.add_argument("--debug", action="store_true", help="Enable debug information (default False)")
    parser.add_argument("--show_graph", action="store_true", help="Show graph of number of requests in each window (default False)")

    args = parser.parse_args()
    analyze_har_file(args.har_path, args.window_time, args.window_increment, args.domain_filter, args.debug, args.show_graph)
