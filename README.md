# HAR Requests Rate Meter

Very simple script that utilizes a sliding window to measure the number of requests per given time window.

Can be utilized for ex. finding a number for an API rate limiting, especially assuming that HAR file can be automatically generated from automatic tests.

## Usage

```
pip install asciichartpy
python ./har_request_rate.py --har_path ./sample_google.har --window_time 2s --window_increment 1s --domain_filter www.google.com --show_graph
```

Sample output:

```
Total requests: 122
Window Time: 2.0 seconds
Window Increment: 1.0 seconds
Average Requests: 23.0
Max requests in a single window: 46
```

### Parameters

* **--har_path** - file path to analyze
* **--window_time** - sliding window size in [time units]
* **--window_increment** - how much window is moved to the next position in [time units] 
* **--debug** - print additional info
* **--domain_filter** - add a domain (ex. google.com) to analyze only those requests
* **--show_graph** - if a graph should be displayed

Time units are provided as: 1s, 1m, 1h.

